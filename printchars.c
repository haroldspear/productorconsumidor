#include "csapp.h"
#include "sbuf.h"
#define SBUFSIZE 10

void *producer(void *);
void *consumer(void *);

sbuf_t sharedBuff;//the struct handles the buffer

int main(int argc, char **argv)
{
	char *filename;	
	struct stat fileStat;
	//id for thread producer and consumer
	pthread_t tid_producer,tid_consumer;	
	
	
	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];

	sbuf_init(&sharedBuff, SBUFSIZE);//init the buff

	if (stat(filename, &fileStat)<0){	
		printf("Archivo %s no fue encontrado\n",filename);
	}
	else{
		printf("Abriendo archivo %s...\n",filename);
		//create the thread for read the file
		Pthread_create(&tid_producer, NULL,producer, filename);	
		Pthread_create(&tid_consumer, NULL,consumer, NULL);	
		Pthread_join(tid_producer, NULL);    
		Pthread_join(tid_consumer, NULL);    
	}
	
	return 0;
}

void *producer(void *filename){
	int fd;
	char c;
	fd = Open(filename, O_RDONLY, 0);
	while(Read(fd,&c,1)){
		sbuf_insert(&sharedBuff,c);//stores c in the buff
		usleep(50*1000);
		//printf("Read: %c\n",c);
	}			
	Close(fd);
		
}
void *consumer(void *vargp){
	char c;
	int k;
	do{	
		c=sbuf_remove(&sharedBuff);//gets c from buff
		sleep(1);
		printf("Read: %c\n",c);
		//gets the value of sem_t items and stores in k
		sem_getvalue(&(sharedBuff.items),&k);
	}while(k>1);
	puts("ending...");
	//free the memory
	sbuf_deinit(&sharedBuff);		
}
