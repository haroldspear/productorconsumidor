/* $begin sbufc */
#include "csapp.h"
#include "sbuf.h"

/* Create an empty, bounded, shared FIFO buffer with n slots */
/* $begin sbuf_init */
void sbuf_init(sbuf_t *sp, int n)
{
    sp->buf = Calloc(n, sizeof(char)); /***/
    sp->n = n;                       /* Buffer holds max of n items */
    sp->front = sp->rear = 0;        /* Empty buffer iff front == rear */
    Sem_init(&sp->mutex, 0, 1);      /* Binary semaphore for locking */
    Sem_init(&sp->slots, 0, n);      /* Initially, buf has n empty slots */
    Sem_init(&sp->items, 0, 0);      /* Initially, buf has zero data items */
}
/* $end sbuf_init */

/* Clean up buffer sp */
/* $begin sbuf_deinit */
void sbuf_deinit(sbuf_t *sp)
{
    Free(sp->buf);
}
/* $end sbuf_deinit */

/* Insert item onto the rear of shared buffer sp */
/* $begin sbuf_insert */
void sbuf_insert(sbuf_t *sp, char item)
{   	
    P(&sp->slots);                          /* Wait for available slot */
    //puts("P(slots) Insert");
    P(&sp->mutex);                          /* Lock the buffer */
    //puts("P(mutex) Insert");    
    sp->buf[(++sp->rear)%(sp->n)] = item;   /* Insert the item */
    //puts("insert in buff");
    V(&sp->mutex);                          /* Unlock the buffer */
    //puts("V(mutex) Insert");
    V(&sp->items);                          /* Announce available item */
    //puts("V(items) Insert\n");
}
/* $end sbuf_insert */

/* Remove and return the first item from buffer sp */
/* $begin sbuf_remove */
char sbuf_remove(sbuf_t *sp)
{
    
    char item;
    P(&sp->items);                          /* Wait for available item */
    //puts("P(items) Remove");
    P(&sp->mutex);                          /* Lock the buffer */
    //puts("P(mutex) Remove");   
    item = sp->buf[(++sp->front)%(sp->n)];  /* Remove the item */
    //puts("remove");
    V(&sp->mutex);                          /* Unlock the buffer */
    //puts("V(mutex) Remove");
    V(&sp->slots);                          /* Announce available slot */
    //puts("V(slots) Remove\n");
    return item;
}
/* $end sbuf_remove */
/* $end sbufc */
